#include <stdio.h>
#include <math.h>
//KALYANGO MARK MAURICE
//23/U/08815/PS

// Function to calculate the discriminant
float calculateDiscriminant(float a, float b, float c) {
    return b * b - 4 * a * c;
}

int main() {
    // Declare variables for coefficients
    float a, b, c;

    // Input coefficients from the user
    printf("Enter the coefficients (a, b, c) of the quadratic equation (ax^2 + bx + c = 0):\n");
    printf("a: ");
    scanf("%f", &a);

    // Validate that 'a' is not zero
    if (a == 0) {
        printf("Invalid input: 'a' cannot be zero .\n");
        return 1; 
    }

    printf("b: ");
    scanf("%f", &b);
    printf("c: ");
    scanf("%f", &c);

    // Calculate the discriminant
    float discriminant = calculateDiscriminant(a, b, c);

    if (discriminant >= 0) {
        // Real roots
        float root1 = (-b + sqrt(discriminant)) / (2 * a);
        float root2 = (-b - sqrt(discriminant)) / (2 * a);

        if (discriminant > 0) {
            // Distinct real roots
            printf("Roots are real and distinct.\n");
            printf("Root 1 = %.2f\n", root1);
            printf("Root 2 = %.2f\n", root2);
        } else {
            // Equal real roots
            printf("Roots are real and equal.\n");
            printf("Root = %.2f\n", root1);
        }
    } else {
        // Complex roots
        printf("Roots are complex.\n");
        float realPart = -b / (2 * a);
        float imaginaryPart = sqrt(-discriminant) / (2 * a);
        printf("Root 1 = %.2f + %.2fi\n", realPart, imaginaryPart);
        printf("Root 2 = %.2f - %.2fi\n", realPart, imaginaryPart);
    }

    return 0;
}
